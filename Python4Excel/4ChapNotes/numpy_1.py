matrix = [[1, 2, 3],
          [4, 5, 6],
          [7, 8, 9]]
# adds one to each element 
print([[i + 1 for i in row] for row in matrix])

import numpy as np
array1 = np.array([10, 100, 1000.])
array2 = np.array([[1., 2., 3.],
                   [4., 5., 6.]])

print(array1.dtype) # gives back the numpy data type
print(array2 * array2) # muliplies and array with itself 
print(array1 * array2) # multiplies two arrrays together 
print(array2 @ array2.T) # array2.T is a shortcut for array2.transpose()

import math
# square roots every element in the array 
print(np.array([[math.sqrt(i) for i in row] for row in array2]))

# using ufunc
print(np.sqrt(array2))

# sum of each collom with ufunc e
print(array2.sum(axis=0))  # Returns a 1d array

print(array2.sum()) # sums the whole array together

print(array1[1]) # Returns a scalar
print(array2[0, 0]) # Returns a scalar
print(array2[:, 1:]) # Returns a 2d array
print(array2[:, 1]) # Returns a 1d array
print(array2[1, :2]) # Returns a 1d array

print(np.arange(2 * 5).reshape(2, 5)) # 2 rows, 5 columns

array3 = np.array([[1., 2., 3.],
                   [4., 5., 6.]])
subset = array3[:, :2]
print(subset) # making subset of array 
subset[0,0] = 10 # adding an element 10 at the beginning of the subset changes the origalal array 
print(subset)
print(array3)

subset = array2[:, :2].copy() # changes subset without changing array 