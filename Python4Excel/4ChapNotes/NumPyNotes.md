# Chapter 4 notes

### NumPy Array
Numpy uses code that was written in C or Fortran—these are
compiled programming languages that are much faster than Python

one-dimensional array has only one axis and
hence does not have an explicit column or row orientation. 

numpy has its own data set (float64)

### Vectorization and Broadcasting

Scalar refers to a basic Python data type like a float or a string. 

If you build the sum of a scalar and a NumPy array, NumPy will perform an elementwise operation. (you don’t have to loop through the elements yourself) The NumPy community refers to this as **vectorization**.

If you use two arrays with different shapes in an arithmetic operation, NumPy extends if possible the smaller array automatically across the larger array so that their shapes become compatible. This is called **broadcasting**

To perform matrix multiplications or dot products, use the @ operator

### Universal Functions (ufunc)

Universal functions (ufunc) work on every element in a NumPy array

The argument axis=0 refers to the axis along the rows while axis=1 refers to the axis along the columns. Leaving the axis argument away sums up the whole array.

### Creating and Manipulating Arrays

When you work with nested lists like matrix you can use **chained indexing** matrix[0][0] will get you the first element of
the first row. 

NumPy arrays you provide the index and slice arguments for both dimensions in a single pair of square brackets 
numpy_array[row_selection, column_selection]
one-dimensional arrays, this simplifies to numpy_array[selection]

slice notation uses a start index (included) and an end index (excluded) with a colon in between, as in start:end 
![Array](Images/Array.png)

create arrays using the arange function
**it is not arrange it stand for Array Range**
.reshape will reshape the array into collom and rows 
.reshape(2, 5)  2 rows and 5 columns 

there are NumPy array functions that can make using arrays easier like .arange or .reshape

if you make a subset of an array and change the values it will change the values in the origanal array as well. To change a subset without chaging the origanal array you use .copy() 
subset = array2[:, :2].copy()

There are two main problems with using NumPy for data analysis. One the whole NumPy array needs to be of the same data type. Two it's hard to know what each column
or row refers to because you typically select columns via their position, such as in
array2[:, 1].




