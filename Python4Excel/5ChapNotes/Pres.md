# Data alignment
![getting data that is not aligned](Images/badData.jpeg)\
The above version of getting data is slow. 

![data alignment](Images/dataA.png)\
32 + 32 + 32 = 96\
32 + 32 + 64 = 128\
A 64 bit of data should start on a number divisible by 64


# Vectorization
![vectorization](Images/vectorization.png)\
If you want to add each row of two columns together instead of getting the data from each row in each column, adding them and then getting the result, you 
can get a chunk of data from each column and then together and print the result making the process much faster. If the data is not aligned then you can't 
vectorize the operation, which is why vectorization and data alignment go together to make the process much faster.
