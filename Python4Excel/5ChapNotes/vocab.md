[Jupyter notebooks](https://en.wikipedia.org/wiki/Project_Jupyter) - The Jupyter Notebook is the original web application for creating and sharing computational documents. It offers a simple, streamlined, document-centric experience. Jupyter Notebook is built using several open-source libraries, including IPython, ZeroMQ, Tornado, jQuery, Bootstrap, and MathJax. A Jupyter Notebook document is a browser-based REPL containing an ordered list of input/output cells which can contain code, text (using Markdown), mathematics, plots and rich media. Underneath the interface, a notebook is a JSON document, following a versioned schema, usually ending with the ".ipynb" extension.

[Pandas](https://en.wikipedia.org/wiki/Pandas_(software)) - A software library written for the Python programming language for data manipulation and analysis. In particular, it offers data structures and operations for manipulating numerical tables and time series.

[Interface](https://en.wikipedia.org/wiki/Interface_(computing)) - a shared boundary across which two or more separate components of a computer system exchange information. The exchange can be between software, computer hardware, peripheral devices, humans, and combinations of these.

[Data sets](https://en.wikipedia.org/wiki/Data_set) - A collection of data. Several characteristics define a data set's structure and properties. These include the number and types of the attributes or variables, and various statistical measures applicable to them.

[Statistics](https://en.wikipedia.org/wiki/Statistics) - Is the discipline that concerns the collection, organization, analysis, interpretation, and presentation of data. 

[Time series](https://en.wikipedia.org/wiki/Time_series) - A series of data points indexed (or listed or graphed) in time order. Most commonly, a time series is a sequence taken at successive equally spaced points in time. Example of time series are heights of ocean tides. 

[Cleaning data](https://en.wikipedia.org/wiki/Data_cleansing) - The process of detecting and correcting (or removing) corrupt or inaccurate records from a record set, table, or database and refers to identifying incomplete, incorrect, inaccurate or irrelevant parts of the data and then replacing, modifying, or deleting the dirty or coarse data.

[Aggregation](https://en.wikipedia.org/wiki/Object_composition#Aggregation) - Data composed of smaller pieces that form a larger whole.

[Descriptive statistics](https://en.wikipedia.org/wiki/Descriptive_statistics) - A summary statistic that quantitatively describes or summarizes features from a collection of information. Is also the process of using and analysing those statistics.

[Visualization](https://en.wikipedia.org/wiki/Data_and_information_visualization) -The graphic representation of data and information. It is a particularly efficient way of communicating when the data or information is numerous as for example a time series.

**Data alignment**\
![getting data that is not aligned](Images/badData.jpeg)\
The above version of getting data is slow. 

![data alignment](Images/dataA.png)\
32 + 32 + 32 = 96\
32 + 32 + 64 = 128\
A 64 bit of data should start on a number divisible by 64


**Vectorization**\
![vectorization](Images/vectorization.png)\
If you want to add each row of two columns together instead of getting the data from each row in each column, adding them and then getting the result, you can get a chunk of data from each column and then together and print the result making the process much faster. If the data is not aligned then you can't vectorize the operation, which is why vectorization and data alignment go together to make the process much faster.
