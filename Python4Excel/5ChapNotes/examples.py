import pandas as pd

data=[["Mark", 55, "Italy", 4.5, "Europe"],
      ["John", 33, "USA", 6.7, "America"],
      ["Tim", 41, "USA", 3.9, "America"],
      ["Jenny", 12, "Germany", 9.0, "Europe"]]
df = pd.DataFrame(data=data,
                   columns=["name", "age", "country",
                            "score", "continent"],
                   index=[1001, 1000, 1002, 1003])
 
print(df)
print(df.info()) 
# By calling the info method, you will get some basic information, 
# most importantly the number of data points and the data types for each column
print(df.dtypes)
print(df.index)
df.index.name = "user_id"
print(df)

print(df.reset_index())
# "reset_index" turns the index into a column, replacing the
# index with the default index. This corresponds to the DataFrame
# from the beginning that we loaded from Excel.

print(df.reset_index().set_index("name"))
# "reset_index" turns "user_id" into a regular column and 
# "set_index" turns the column "name" into the index

# when calling a DateFrame method it dosent change the origanal value
# if you want to change the original DateFrame you would have to assign the
# return value back to the original variable like the following: df = df.reset_index()

print(df.reindex([999, 1000, 1001, 1004]))
# To change the index, use the reindex method

print(df.sort_index())
# to sort an index, use the sort_index method

print(df.sort_values(["continent", "age"]))
# to sort the rows by one or more columns, use sort_values

print(df.columns)

df.columns.name = "properties"
print(df)

print(df.rename(columns={"name": "First Name", "age": "Age"}))
# renaming columns 

print(df.drop(columns=["name", "country"],
              index=[1000, 1003]))

print(df.T) # Shortcut for df.transpose()
