import pandas as pd

data = pd.read_excel("Ite140/Datasets/TrackConditions.xlsx")

pivot = pd.pivot_table(data,
        index="weather", columns="race",
        values="low_temp", aggfunc="mean",
        margins=True, margins_name="TOTAL")
print(pivot)

pivot_2 = pd.pivot_table(data,
        index="weather", columns="race",
        values="high_temp", aggfunc="mean",
        margins=True, margins_name="TOTAL")
print(pivot_2)

pivot_3 = pd.pivot_table(data,
        index="weather", columns="race",
        values="precipitation_24hrs", aggfunc="mean",
        margins=True, margins_name="TOTAL")
print(pivot_3)
