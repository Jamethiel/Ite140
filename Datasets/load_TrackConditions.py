import pandas as pd

data = pd.read_excel("Ite140/Datasets/TrackConditions.xlsx")
df = pd.DataFrame(data=data)
print(df)

print(df.info()) 
print(df.dtypes)
print(df.index)

df.index.name = "user_id"
print(df)

print(df.reset_index())
print(df.reset_index().set_index("race"))

print(df.sort_values(["race", "year"]))

print(df.columns)

df.columns.name = "properties"
print(df)


print(df.drop(columns=["attendance"]))

print(df.T) # Shortcut for df.transpose()
# columns into rows and vice versa

print(df.loc[:, ["race", "year", "track_condition", "weather", "high_temp", "low_temp", "low_temp", "precipitation_24hrs", "attendance"]])