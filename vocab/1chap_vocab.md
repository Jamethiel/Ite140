# PostgreSQL

PostgreSQL is a powerful, open source object-relational database system that extends the SQL language combined with many features that safely store and scale the most complicated data workloads.

- Runs on all major operating systems
- Powerful add-ons such as the popular PostGIS (adds support for geographic objects)
- Highly extensible. For example, you can define your own data 
  types, build out custom functions, even write code from different programming 
  languages without recompiling your database
- ACID-compliant since 2001

## ACID

**A - Atonomy**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All or nothing transactions 

**C - Consistency**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Guarentees committed transaction state

**I - Isolation**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Transactions are indepemdant 

**D - Durability**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Commited data is never lost

## Origins

PostgreSQL dates back to 1986 as part of the POSTGRES project at 
the University of California at Berkeley and has more than 35 years of active 
development on the core platform.

In 1994, Andrew Yu and Jolly Chen added an SQL language interpreter to POSTGRES. Under a new name, Postgres95 was subsequently released to the web to find its own way in the world as an open-source descendant of the original POSTGRES Berkeley code.

By 1996, it became clear that the name “Postgres95” would not stand the test of time. A new name was chosen PostgreSQL, to reflect the relationship between the original POSTGRES and the more recent versions with SQL capability.  

&nbsp;  
&nbsp;  
&nbsp; 
&nbsp; 
&nbsp; 
&nbsp; 

# Amazon Snowball

## How it's used in the text?

"'I would have thought we would have all of this on <mark>Amazon Snowball</mark>,
Google BigTable or... some other cloud service,' I say, curious."(A Curious
Moon pg.3)

## Description:

AWS says that "Snowball is a [petabyte-scale](https://en.wikipedia.org/wiki/Byte#Multiple-byte_units) data transport solution that
uses secure appliances to transfer large amounts of data into and out of the
AWS cloud. Using Snowball addresses common challenges with large-scale data 
transfers including high network costs, long transfer times, and security
concerns."

## My understaning:

Amazon snowball is a service that provides secure, rugged devices, so you
can bring AWS computing and storage capabilities to your edge environments, 
and transfer data into and out of AWS. 

## Why is it used?

The AWS Snowball service uses physical storage devices to transfer large 
amounts of data between Amazon Simple Storage Service [Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3) and your
onsite data storage location at faster-than-internet speeds.